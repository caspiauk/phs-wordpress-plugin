jQuery(function ($) {
    if (window.google) {
        new google.maps.places.Autocomplete($('#phs_pickup')[0]);
        new google.maps.places.Autocomplete($('#phs_destination')[0]);
    }

    if ($.ui.datepicker) {
    	$('.phs-row-dates-jquery #phs_start_date').datepicker({
    		dateFormat: 'dd/mm/yy'
    	});
    }
});