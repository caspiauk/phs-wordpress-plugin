<div class="phs-wrap">
	<form action="" method="post">
		<?php if (count($this->form_errors) > 0): ?>
			<p><?php 
			foreach ($this->form_errors as $key => $value):
				echo $key.': '.$value.'<br />';
			endforeach;
			?>
			</p>
		<?php endif; ?>
		<div class="phs-row">
		<label>Pickup</label>
		<input type="text" name="freephsform[pickup]" id="phs_pickup" placeholder="Pickup" value="<?php isset($this->form_data['pickup']) && print esc_attr($this->form_data['pickup']) ?>" />
		</div>
		<div class="phs-row">
		<label>Destination</label>
		<input type="text" name="freephsform[destination]" id="phs_destination" placeholder="Drop off" value="<?php isset($this->form_data['destination']) && print esc_attr($this->form_data['destination']) ?>" />
		</div>
		<div class="phs-row phs-row-selects">
		<label>Date</label>
			<?php if ($this->options['jquery_datepickers']): ?>
				<div class="phs-row-dates phs-row-dates-jquery">
					<input type="text" name="freephsform[start_date][date]" id="phs_start_date" placeholder="Start Date" value="<?php isset($this->form_data['start_date']['date']) && print esc_attr($this->form_data['start_date']['date']) ?>" />
				</div>
			<?php else: ?>
				<div class="phs-row-dates">
					<select name="freephsform[start_date][year]">
						<?php 
						$selected = isset($this->form_data['start_date']['year']) ? intval($this->form_data['start_date']['year']) : '';
						foreach (range(date('Y'), date('Y')+3) as $value): ?>
						<option value="<?php echo $value ?>" <?php selected($selected, $value) ?>><?php echo $value ?></option>
						<?php endforeach; ?>
					</select>
					<select name="freephsform[start_date][month]">
						<?php 
						$selected = isset($this->form_data['start_date']['month']) ? esc_attr($this->form_data['start_date']['month']) : '';
						foreach (range(1, 12) as $value):
						$month = date('M', mktime(0, 0, 0, $value, 10)); 
						$value = str_pad($value, 2, '0', STR_PAD_LEFT);
						?>
						<option value="<?php echo $value ?>" <?php selected($selected, $value) ?>><?php echo $month ?></option>
						<?php endforeach; ?>
					</select>
					<select name="freephsform[start_date][day]">
						<?php 
						$selected = isset($this->form_data['start_date']['day']) ? esc_attr($this->form_data['start_date']['day']) : '';
						foreach (range(1, 31) as $value): 
						$value = str_pad($value, 2, '0', STR_PAD_LEFT); ?>
						<option value="<?php echo $value ?>" <?php selected($selected, $value) ?>><?php echo $value ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			<?php endif; ?>
			<div class="phs-row-hours">
				<select name="freephsform[start_date][hours]">
					<?php 
					$selected = isset($this->form_data['start_date']['hours']) ? esc_attr($this->form_data['start_date']['hours']) : '';
					foreach (range(0, 23) as $value): 
					$value = str_pad($value, 2, '0', STR_PAD_LEFT); ?>
					<option value="<?php echo $value ?>" <?php selected($selected, $value) ?>><?php echo $value ?></option>
					<?php endforeach; ?>
				</select>
				:
				<select name="freephsform[start_date][minutes]">
					<?php 
					$selected = isset($this->form_data['start_date']['minutes']) ? esc_attr($this->form_data['start_date']['minutes']) : '';
					foreach (array(0, 1, 15, 30, 45) as $value): 
					$value = str_pad($value, 2, '0', STR_PAD_LEFT); ?>
					<option value="<?php echo $value ?>" <?php selected($selected, $value) ?>><?php echo $value ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="phs-row">
		<label>Your Name</label>
		<input type="text" name="freephsform[first_name]" placeholder="First Name" value="<?php isset($this->form_data['first_name']) && print esc_attr($this->form_data['first_name']) ?>" />
		</div>
		<div class="phs-row">
		<label>Email</label>
		<input type="email" name="freephsform[email]" placeholder="Email Address" value="<?php isset($this->form_data['email']) && print esc_attr($this->form_data['email']) ?>" />
		</div>
		<div class="phs-row">
		<label>Phone</label>
		<input type="text" name="freephsform[phone]" placeholder="Phone Number" value="<?php isset($this->form_data['phone']) && print esc_attr($this->form_data['phone']) ?>" />
		</div>
		<div class="phs-btn-actions">
			<button type="submit">Get Instant Quote</button>
		</div>
	</form>
</div>
