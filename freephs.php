<?php
/*
Plugin Name: FreePHS
Plugin URI: https://bitbucket.org/caspiauk/phs-wordpress-plugin
Description: Wordpress plugin to iteract with FreePHS - Free Private Hire System.
Author: FreePHS.
Author URI: http://freephs.com
Version: 0.5
License: GPLv2 or later
*/

if (!defined( 'ABSPATH' )) {
    exit('can not access directly'); // Exit if accessed directly
}

if (is_admin()) {
    require __DIR__.'/FreePHS_Admin.php';

    $freephsAdmin = new FreePHS_Admin(array(
        'rest_url' => '',
        'api_key' => '',
        'jquery_datepickers' => false,
        'paypal_enabled' => false,
        'paypal_test_mode' => false,
        'paypal_email' => '',
        'paypal_currency' => 'GBP',
    ));

    // register activation/deactivation hooks
	register_activation_hook(__FILE__, array($freephsAdmin, 'plugin_activation'));
	register_deactivation_hook(__FILE__, array($freephsAdmin, 'plugin_deactivation'));

	add_filter('plugin_action_links_'.plugin_basename(__FILE__), array($freephsAdmin, 'add_action_links'));
    add_action('admin_menu', array($freephsAdmin, 'add_plugin_page'));
    add_action('admin_init', array($freephsAdmin, 'page_init'));

} else {
    // frontend
    require __DIR__.'/FreePHS_Shortcode.php';
    require __DIR__.'/FreePHS_ApiClient.php';
    $freephsShortcode = new FreePHS_Shortcode();

    add_action('wp', array($freephsShortcode, 'wp_hook'));
    add_action('wp_print_styles', array($freephsShortcode, 'print_styles'));
}