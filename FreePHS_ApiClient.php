<?php

/**
 * @author FreePHS support@freephs.com
 */
class FreePHS_ApiClient
{
    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @param string $baseUrl
     */
    public function __construct($baseUrl, $apiKey)
    {
        $this->baseUrl = $baseUrl;
        $this->apiKey = $apiKey;
    }

    /**
     */
    public function getVehicleCats()
    {
        $response = wp_remote_get($this->baseUrl.'/vehicle/cats?apikey='.$this->apiKey);

        return $response;
    }

    /**
     */
    public function newCustomer(array $data)
    {
        $response = wp_remote_post($this->baseUrl.'/customer/?apikey='.$this->apiKey, array(
            'body' => array('phs_rest_new_customer' => $data),
        ));

        return $response;
    }

    /**
     */
    public function newQuote(array $data)
    {
        $response = wp_remote_post($this->baseUrl.'/quote/?apikey='.$this->apiKey, array(
            'body' => array('phs_rest_new_quote' => $data),
        ));

        return $response;
    }

    /**
     */
    public function updateQuote($id, array $data)
    {
        $response = wp_remote_request($this->baseUrl.'/quote/'.$id.'?apikey='.$this->apiKey, array(
            'method' => 'PUT',
            'body' => array('phs_rest_edit_quote' => $data),
        ));

        return $response;
    }

    /**
     */
    public function getQuote($url)
    {
        if (is_numeric($url)) {
            $url = $this->baseUrl.'/quote/'.$url;
        }

        $response = wp_remote_get($url.'?apikey='.$this->apiKey);

        return $response;
    }
}
