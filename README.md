Installation
======
- Download archive from Downloads section of this repository
- Unpack archive to "freephs" directory
The contents of the directory should look like
```
.. /freephs/
..... freephs.php
...... FreePHS_Admin ...
...... FreePHS_ApiClient ...
```

- Move this directory to your wordpress plugins directory .. /wp-content/plugins/freephs
- Go to WordPress admin and activate the plugin.
- Go to Settings -> FreePHS and enter your authentification details:
  	* **Rest Url**: http://yourdomain.freephs.com/api
  	* **Api Key**: Your Api Key (Take your api key from FreePHS admin -> Company Details)

Adding Quotation Form
======
- To display quotation form simply put ```[freephs]``` shortcode to a post content.

![FreePHS Form](/caspiauk/phs-wordpress-plugin/raw/master/freephsform.png)
