=== FreePHS ===
Contributors: freephs
Tags: freephs.
Requires at least: 3.1
Tested up to: 4.2
Stable tag: 0.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Wordpress plugin to iteract with FreePHS - Free Private Hire System.

== Description ==

Wordpress plugin to iteract with FreePHS - Free Private Hire System.
More details - [Official Repository page](https://bitbucket.org/caspiauk/phs-wordpress-plugin/)
Support - [Issue Tracker](https://bitbucket.org/caspiauk/phs-wordpress-plugin/issues)

== Installation ==

1. Upload the FreePHS plugin to your blog, Activate it, Go to plugin Settings.
Enter:
- Rest Url: http://yourdomain.freephs.com/api
- Api Key: Your ApiKey obtained from FreePHS system.
2. Insert ```[freephs]``` shortcode within your post content.

== Screenshots ==
freephsform.png

== Changelog ==

= 0.4.1 =
* readme.txt fix

= 0.4 =
* 3.1 Compability

= 0.3 =
* Code cleanup, minor css fixes.

= 0.2 =
* CSS fixes.

= 0.1 =
* First Release