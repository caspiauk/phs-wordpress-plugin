<?php

/**
 * @author FreePHS support@freephs.com
 */
class FreePHS_Shortcode
{
    /**
     * @var string
     */
    const SHORTCODE_NAME = 'freephs';

    /**
     * @var FreePHS_Client
     */
    private $client;

    /**
     * @var string
     */
    private $options;

    /**
     * @var string
     */
    private $action;

    /**
     * @var array
     */
    private $form_data;

    /**
     * @var array
     */
    private $form_errors;

    /**
     * @var array
     */
    private $vehicle_cats;

    /**
     * @var string
     */
    private $quote_id;

    /**
     * @var stdClass
     */
    private $quote;

    /**
     */
    public function __construct()
    {
        $this->options = array_merge(
            array(
                'rest_url' => '',
                'api_key' => '',
                'jquery_datepickers' => false,
                'paypal_enabled' => false,
            ),
            get_option('freephs', array())
        );
        
        $this->client = new FreePHS_ApiClient(
            $this->options['rest_url'],
            $this->options['api_key']
        );

        $this->form_data = array();
        $this->form_errors = array();
        $this->vehicle_cats = array();
    }

    public function print_styles()
    {
        wp_register_style('freephs-styles', plugin_dir_url(__FILE__).'/styles.css');
        wp_enqueue_style('freephs-styles');
        wp_register_script('google-places', 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places');
        wp_enqueue_script('google-places');

        $deps = array('jquery', 'google-places');
        if ($this->options['jquery_datepickers']) {
            $deps[] = 'jquery-ui-datepicker';
        }
        wp_register_script('freephs-scripts', plugin_dir_url(__FILE__).'/scripts.js', $deps);
        wp_enqueue_script('freephs-scripts');
    }

    public function wp_hook()
    {
        global $post;
        if (false !== strpos($post->post_content, '['.self::SHORTCODE_NAME)) {
            add_shortcode(self::SHORTCODE_NAME, array($this, 'print_shortcode_content'));

            $this->action = isset($_GET['_phs']) ? $_GET['_phs'] : 'journey_details';
            switch ($this->action) {
                case 'journey_details':
                    $this->journey_details_hook();
                    break;
                case 'vehicle':
                    $this->vehicle_list_hook();
                    break;
                case 'pay':
                    $this->book_and_pay_hook();
                    break;
            }
        }
    }

    /**
     */
    public function print_shortcode_content() 
    {
        if ($this->action == 'journey_details') {
            require(__DIR__.'/freephs_journey_details_form.php');
        } elseif ($this->action == 'vehicle') {
            require(__DIR__.'/freephs_select_vehicle_list.php');
        } elseif ($this->action == 'pay') {
            require(__DIR__.'/freephs_book_and_pay.php');
        } elseif ($this->action == 'pp_processed') {
            require(__DIR__.'/freephs_paypal_processed.php');
        }
    }

    protected function journey_details_hook() 
    {
        if (!empty($_POST['freephsform'])) {
            $this->form_data = $_POST['freephsform'];
            $this->form_errors = $this->validate_form_data($this->form_data);
            if (count($this->form_errors) == 0) {
                $submit_data = $this->prepare_form_data($this->form_data);
                $response = $this->client->newQuote($submit_data);
                $code = wp_remote_retrieve_response_code($response);
                if ($code == 201) {
                    $location_url = wp_remote_retrieve_header($response, 'location');
                    $response = $this->client->getQuote($location_url);
                    $code = wp_remote_retrieve_response_code($response);
                    if ($code == 200) {
                        $body = json_decode(wp_remote_retrieve_body($response));
                        $quote = $body->quote;

                        wp_redirect(add_query_arg(array('_phs' => 'vehicle', '_phs_id' => $quote->id)));
                        exit();
                    }
                } else {
                    $this->form_errors = $this->response_errors($response);
                }
            }
        }
    }

    public function vehicle_list_hook()
    {
        if (!empty($_GET['_phs_id'])) {
            $this->quote_id = intval($_GET['_phs_id']);

            if (!empty($_GET['_phs_vid'])) {
                $data['vehicleCats'] = array(intval($_GET['_phs_vid']));

                $response = $this->client->updateQuote($this->quote_id, $data);
                $code = wp_remote_retrieve_response_code($response);
                if ($code == 204) {
                    wp_redirect(add_query_arg(array('_phs' => 'pay', '_phs_id' => $this->quote_id)));
                    exit();
                } else {
                    print_r($response);die;
                    $this->form_errors = $this->response_errors($response);
                }
            }

            $response = $this->client->getVehicleCats();
            $code = wp_remote_retrieve_response_code($response);
            if ($code == 200) {
                $body = json_decode(wp_remote_retrieve_body($response));
                $this->vehicle_cats = $body->cats;
            } else {
                $this->form_errors = $this->response_errors($response);
            }
        }
    }

    public function book_and_pay_hook()
    {
        if (!empty($_GET['_phs_id'])) {
            $this->quote_id = intval($_GET['_phs_id']);

            $response = $this->client->getQuote($this->quote_id);
            $code = wp_remote_retrieve_response_code($response);
            if ($code == 200) {
                $body = json_decode(wp_remote_retrieve_body($response));
                $this->quote = $body->quote;
            } else {
                $this->form_errors = $this->response_errors($response);
            }
        }
    }

    public function validate_form_data($form_data)
    {
        $errors = array();
        if (empty($form_data['pickup'])) {
            $errors['Pickup'] = 'This field is required';
        }

        if (empty($form_data['destination'])) {
            $errors['Destination'] = 'This field is required';
        }

        if (empty($form_data['start_date']['year']) && empty($form_data['start_date']['date'])) {
            $errors['Start Date'] = 'This field is required';
        }

        if (empty($form_data['first_name'])) {
            $errors['First Name'] = 'This field is required';
        }

        if (empty($form_data['email'])) {
            $errors['Email'] = 'This field is required';
        } else {
            if (!is_email($form_data['email'])) {
                $errors['Email'] = 'This address is invalid';
            }
        }

        if (empty($form_data['phone'])) {
            $errors['Phone'] = 'This field is required';
        }

        return $errors;
    }

    protected function prepare_form_data($form_data)
    {
        $data = array(
            'pickup' => $form_data['pickup'],
            'destination' => $form_data['destination'],
            'startDate' => $form_data['start_date'],
            'customer' => array(
                'firstName' => $form_data['first_name'],
                'email' => $form_data['email'],
                'phone' => $form_data['phone'],
            )
        );

        if (isset($form_data['start_date']['year'])) {
            $data['startDate'] = sprintf('%s-%s-%s %s:%s:00', 
                empty($form_data['start_date']['year']) ? '1970' : $form_data['start_date']['year'],
                empty($form_data['start_date']['month']) ? '0' : $form_data['start_date']['month'],
                empty($form_data['start_date']['day']) ? '0' : $form_data['start_date']['day'],
                empty($form_data['start_date']['hours']) ? '0' : $form_data['start_date']['hours'],
                empty($form_data['start_date']['minutes']) ? '0' : $form_data['start_date']['minutes']
            );
        } elseif (isset($form_data['start_date']['date'])) {
            $data['startDate'] = sprintf('%s %s:%s:00', 
                \DateTime::createFromFormat('d/m/Y', $form_data['start_date']['date'])->format('Y-m-d'),
                empty($form_data['start_date']['hours']) ? '0' : $form_data['start_date']['hours'],
                empty($form_data['start_date']['minutes']) ? '0' : $form_data['start_date']['minutes']
            );
        }

        return $data;
    }

    private function response_errors($response)
    {
        if (is_wp_error($response)) {
            return array($response->get_error_message());
        } 

        $code = wp_remote_retrieve_response_code($response);
        if ($code == 400) {
            $body = json_decode(wp_remote_retrieve_body($response));
            return $this->form_errors_to_array($body->errors);
        } else {
            return array('Error: '. wp_remote_retrieve_response_code($response).' '. wp_remote_retrieve_response_message($response));
        }
    }
    
    private function form_errors_to_array($response)
    {
        $errors = array();
        $response = (array) $response;
        foreach ($response as $key => $value) {
            if (is_string($value)) {
                $errors[] = $value;
            } elseif (is_array($value)) {
                $errors[$key] = $value[0];
            } elseif (is_object($value)) {
                foreach ($value as $k => $v) {
                    $errors[$k] = $v[0];
                }
            }
        }

        return $errors;
    }
}