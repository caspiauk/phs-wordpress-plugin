<div class="phs-wrap">
	<p class="phs-price">
		PRICE: <strong><?php echo $this->quote->price ?></strong>
	</p>
	<?php if($this->options['enable_paypal']): ?>
		<form method="post" action="<?php echo ($this->options['paypal_test_mode']) ? 'https://www.sandbox.paypal.com/cgi-bin/webscr' : 'https://www.paypal.com/cgi-bin/webscr' ?>">
		    <input type="hidden" name="cmd" value="_xclick" />
		    <input type="hidden" name="business" value="<?php echo $this->options['paypal_email'] ?>" />
		    <input type="hidden" name="item_name" value="Booking #<?php echo $this->quote->id ?>" />
		    <input type="hidden" name="item_number" value="<?php echo $this->quote->id ?>" />
		    <input type="hidden" name="amount" value="<?php echo number_format($this->quote->price, 2, '.', '') ?>" />
		    <input type="hidden" name="no_shipping" value="0" />
		    <input type="hidden" name="currency_code" value="<?php echo $this->options['paypal_currency'] ?>" />
		    <input type="hidden" name="return" value="<?php echo add_query_arg(array('_phs' => 'pp_processed'), the_permalink()) ?>" />
		    <input type="hidden" name="notify_url" value="<?php echo plugin_dir_url(__FILE__).'paypal.php?'.http_build_query(array('id' => $this->quote->id)) ?>" />
		    <input type="hidden" name="cancel_return" value="<?php echo add_query_arg(array('_phs' => 'cancel'), the_permalink()) ?>" />
		    <div class="phs-btn-actions">
		    	<button type="submit">Pay Via PayPal &gt;</button>
		    </div>
		</form>
	<?php endif; ?>
</div>
