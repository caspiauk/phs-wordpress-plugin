<?php

/**
 * @author FreePHS support@freephs.com
 */
class FreePHS_Admin
{
    /**
     * @var array|null
     */
    protected $options;

    public function __construct(array $default_options = array())
    {
        $this->options = $default_options;
    }

    /**
     * Activation hook
     */
    public function plugin_activation()
    {
        add_option('freephs', $this->options);
    }

    /**
     * Deactivation hook
     */
    public function plugin_deactivation()
    {
        delete_option('freephs');
    }

    public function add_action_links($links)
    {
        $pluginLinks = array(
            '<a href="'.admin_url('options-general.php?page=freephs').'">'.__('Settings', 'freephs').'</a>',
        );

        return array_merge($links, $pluginLinks);
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        add_options_page(
            'Settings Admin',
            'FreePHS',
            'manage_options',
            'freephs',
            array($this, 'create_admin_page')
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        ?>
        <div class="wrap">
            <?php screen_icon();
        ?>
            <h2>FreePHS Plugin Settings</h2>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields('freephs');
                do_settings_sections('freephs');
                submit_button();
            ?>
            </form>
        </div>
        <?php

    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        $this->options = get_option('freephs');

        add_settings_section(
            'freephs-settings', // ID
            'Rest/Api Settings', // Title
            array($this, 'print_section_info'), // Callback
            'freephs' // Page
        );

        add_settings_field(
            'rest_url', // ID
            'Rest URL:', // Title
            array($this, 'rest_url_field'), // Callback
            'freephs', // Page
            'freephs-settings' // Section
        );

        add_settings_field(
            'api_key', // ID
            'Api Key:', // Title
            array($this, 'apikey_field'), // Callback
            'freephs', // Page
            'freephs-settings' // Section
        );

        register_setting(
            'freephs', // Option group
            'freephs', // Option name
            array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
            'freephs-form', // ID
            'Form Settings', // Title
            null,
            'freephs' // Page
        );

        add_settings_field(
            'jquery_datepickers', // ID
            'Use jQuery Datepickers:', // Title
            array($this, 'jquery_datepickers_field'), // Callback
            'freephs', // Page
            'freephs-form' // Section
        );

        add_settings_section(
            'freephs-payment', // ID
            'Payment Settings', // Title
            null,
            'freephs' // Page
        );

        add_settings_field(
            'enable_paypal', // ID
            'Enable PayPal:', // Title
            array($this, 'enable_paypal_field'), // Callback
            'freephs', // Page
            'freephs-payment' // Section
        );

        add_settings_field(
            'paypal_test_mode', // ID
            'PayPal Sandbox(Test Mode):', // Title
            array($this, 'paypal_test_mode_field'), // Callback
            'freephs', // Page
            'freephs-payment' // Section
        );

        add_settings_field(
            'paypal_email', // ID
            'PayPal Email:', // Title
            array($this, 'paypal_email_field'), // Callback
            'freephs', // Page
            'freephs-payment' // Section
        );

        add_settings_field(
            'paypal_currency', // ID
            'PayPal Currency:', // Title
            array($this, 'paypal_currency_field'), // Callback
            'freephs', // Page
            'freephs-payment' // Section
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input)
    {
        $newInput = array();
        if (isset($input['rest_url'])) {
            $newInput['rest_url'] = rtrim(sanitize_text_field($input['rest_url']), '/');
        }

        if (isset($input['api_key'])) {
            $newInput['api_key'] = sanitize_text_field($input['api_key']);
        }

        if (isset($input['jquery_datepickers'])) {
            $newInput['jquery_datepickers'] = (bool) $input['jquery_datepickers'];
        } else {
            $newInput['jquery_datepickers'] = false;
        }

        if (isset($input['enable_paypal'])) {
            $newInput['enable_paypal'] = (bool) $input['enable_paypal'];
        } else {
            $newInput['enable_paypal'] = false;
        }

        if (isset($input['paypal_test_mode'])) {
            $newInput['paypal_test_mode'] = (bool) $input['paypal_test_mode'];
        } else {
            $newInput['paypal_test_mode'] = false;
        }

        if (isset($input['paypal_email'])) {
            $newInput['paypal_email'] = sanitize_text_field($input['paypal_email']);
        }

        if (isset($input['paypal_currency'])) {
            $newInput['paypal_currency'] = sanitize_text_field($input['paypal_currency']);
        }

        return $newInput;
    }

    /**
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your settings below:';
    }

    /**
     * Display field
     */
    public function rest_url_field()
    {
        printf(
            '<input type="text" name="freephs[rest_url]" value="%s" />',
            isset($this->options['rest_url']) ? esc_attr($this->options['rest_url']) : ''
        );
    }

    /**
     * Display field
     */
    public function apikey_field()
    {
        printf(
            '<input type="text" name="freephs[api_key]" value="%s" />',
            isset($this->options['api_key']) ? esc_attr($this->options['api_key']) : ''
        );
    }

    /**
     * Display field
     */
    public function jquery_datepickers_field()
    {
        printf(
            '<input type="checkbox" name="freephs[jquery_datepickers]" value="1" %s />',
            !empty($this->options['jquery_datepickers']) ? 'checked' : ''
        );
    }

    /**
     * Display field
     */
    public function enable_paypal_field()
    {
        printf(
            '<input type="checkbox" name="freephs[enable_paypal]" value="1" %s />',
            !empty($this->options['enable_paypal']) ? 'checked' : ''
        );
    }

    /**
     * Display field
     */
    public function paypal_test_mode_field()
    {
        printf(
            '<input type="checkbox" name="freephs[paypal_test_mode]" value="1" %s />',
            !empty($this->options['paypal_test_mode']) ? 'checked' : ''
        );
    }

    /**
     * Display field
     */
    public function paypal_email_field()
    {
        printf(
            '<input type="email" name="freephs[paypal_email]" value="%s" />',
            isset($this->options['paypal_email']) ? esc_attr($this->options['paypal_email']) : ''
        );
    }

    /**
     * Display field
     */
    public function paypal_currency_field()
    {
        $selected = isset($this->options['paypal_currency']) ? $this->options['paypal_currency'] : '';
        printf(
            '<select name="freephs[paypal_currency]">
                <option value="GBP" %s>GBP</option>
                <option value="USD" %s>USD</option>
            </select>',
            selected($selected, 'GBP', false),
            selected($selected, 'USD', false)
        );
    }
}
